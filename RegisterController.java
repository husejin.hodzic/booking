package sample;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

import javax.swing.*;
import javax.xml.crypto.dsig.spec.XSLTTransformParameterSpec;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;

public class RegisterController implements Initializable {

@FXML
private ChoiceBox<String> tipKorisnika;
    @FXML
    private TextField txtIme;
    @FXML
    private TextField txtPrezime;
    @FXML
    private TextField txtKorisnickoIme;
    @FXML
    private PasswordField txtLozinka;
    @FXML
    private PasswordField txtLozinkaPotvrda;
    @FXML
    private TextField txtEmail;
    @FXML
    private TextField txtBrojTelefona;
    @FXML
    private Button btnRegisterq;
    ObservableList<String> tipoviKorisnika = FXCollections.observableArrayList();
//treba jos za dropdown menu kad ga napravim
static int indx=0;
    @FXML
    public void registerButtonListener(ActionEvent event)
    {

        try{
            Connection conn = DBConnector.getConnection();
            String ime=txtIme.getText();
            String prezime=txtPrezime.getText();
            System.out.println(ime+prezime);
            String lozinka=txtLozinka.getText();
            String potvrdaLozinke=txtLozinkaPotvrda.getText();
            String Email=txtEmail.getText();
            String BrojTelefona=txtBrojTelefona.getText();
            String korisnickoIme=txtKorisnickoIme.getText();
            String tipKorisnikatxt=(String)tipKorisnika.getValue();

            //Provjeri lozinka jel se podudara
            //Provjeri jel vec postoji taj username sa tim passwordom
            //ako nema onda dole ovo sve
            String registerUser = "INSERT INTO korisnik VALUES (null,?,?,?,?,?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(registerUser);
            stmt.setString(1,korisnickoIme);
            stmt.setString(2,lozinka);
            stmt.setString(3,ime);
            stmt.setString(4,prezime);
            stmt.setString(5,tipKorisnikatxt);
            stmt.setString(6,Email);
            stmt.setString(7,BrojTelefona);
            stmt.execute();
            TimeUnit.SECONDS.sleep(1);
            vratiNatrag(event);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }


        public
        void vratiNatrag(ActionEvent event) throws IOException
        {
            Parent root = FXMLLoader.load(getClass().getResource("prijava.fxml"));
            Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
            stage.setScene(new Scene(root));
            stage.setResizable(false);
            stage.show();
        }


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
tipoviKorisnika.add("Korisnik");
        tipoviKorisnika.add("Izdavac");
        tipKorisnika.setValue("Korisnik");
        tipKorisnika.setItems(tipoviKorisnika);
    }
}
